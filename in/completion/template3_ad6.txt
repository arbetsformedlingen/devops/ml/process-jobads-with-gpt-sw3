Deltid / Ströpass Kungsbacka v.04-14 Är du en legitimerad Sjuksköterska som vill ha en trygg anställning med bra villkor? Perfekt! Då har du kommit helt rätt.

Just nu söker vi Magnifiqa Sjuksköterskor till spännande uppdrag på en Vårdavdelning i Kungsbacka

Sedvanliga
arbetsuppgifter på en Slutenvårdsavdelning med inriktning inom Rehabilitering, Geriatrik, Postoperativ vård och Palliativ vård. Verksamheten använder journalsystemen VAS och NCS Cross.

Deltidstjänstgöring med ca. 3 arbetspass/ vecka under perioden 24-01-22 till 24-03-31

Du som läser detta hoppas vi är legitimerad Sjuksköterska och har några års erfarenhet från yrket. Som person tror vi att du är glad, trivs med att träffa nya människor och vill axla rollen som ambassadör ute på uppdrag hos våra kunder.

Magnifiq - det familjära vårdbemanningsbolaget

Magnifiq är ingen vårdjätte. Istället bygger vår framgång på nöjda medarbetare med trygga anställningar och tjänster av högsta kvalitet.

Idag jobbar vi med så gott som alla regioner, kommuner och privata vårdgivare över hela Sverigekartan och vår ambition har alltid varit att kunna erbjuda våra konsulter bästa tänkbara uppdrag, trygga villkor och höga ersättningar oavsett var i landet du önskar arbeta.

Som konsult hos oss får du bland annat;

- En personlig kontakt som alltid finns tillgänglig för dig
- En konkurrenskraftig lön
- Försäkring, tjänstepension och sjuklön
- Reseersättning och boende utanför hemorten

Låter detta som något för dig? Det både tror och hoppas vi.
Vänta i så fall inte - uppdraget tillsätts löpande.

Varmt välkommen!

Teamet på Magnifiq

Växel: 090-136200

Det är en jobbannons. Du är en expert på att analysera jobbannonser och att skriva texter begripligt på lättläst svenska.
Identifiera meningar som är svåra att förstå och skriv om de meningar som inte är skrivna på lättläst svenska
så att de blir enklare att förstå.
Ett värde sätts i resultatet: lattlast_mening: en array som innehåller den ursprungliga meningen
och den mening som är omskriven på lättläst svenska.
Ursprungliga meningar och omskrivna meningar ska vara avskilda med =>

Så här ser resultatet ut av att bearbeta annonsen i enlighet med detta:
