Däckskiftare sökes i Upplands Väsby! Om jobbet:

Till kommande däcksäsong söker vi däckskiftare med placering hos våra kunder i Stockholm. Bland våra kunder finns de ledande återförsäljarna så som Volvo med flera. Här får du chansen att jobba med att bidra till säkerheten på vägarna i några av Stockholms mest moderna däck- och bilverkstäder.

Som däckskiftare jobbar du i verkstaden med att ta emot kunder och deras bilar för att byta däck från sommar till vinterdäck. Under däcksäsongen arbetar man med hög noggrannhet för att leverera kvalité i ett högt tempo.

Vi erbjuder två dagars utbildning för att du ska vara redo att jobba på ett säkert och tryggt sätt ute hos våra kunder.

Din profil:

Som person är du social, kommunikativ och en lagspelare. Du arbetar på ett strukturerat arbetssätt med god prioriteringsförmåga samtidigt som du är noggrann i ditt arbete. Att ge service till kunder är något du ser som en självklarhet samtidigt som du klarar av att arbeta i högt tempo.

Intresserad? Skicka in din ansökan snarast då urval sker löpande. Vid eventuella frågor om tjänsten hänvisar vi till ansvarig rekryteringskonsult på Aura Personal.

Anställningen är på heltid med start i mitten på mars men vissa tjänster kan komma att tillsättas redan nu. Urval kommer dock att påbörjas omgående så du gör därför rätt i att ansöka så snart som möjligt!

Välkommen med din ansökan!

Om oss:

Aura personal tror på människor och att alla som vill kan. Våra år i branschen har gjort oss övertygade om att många fler kan skapa sin egen framgång bara någon ser dem och ger dem chansen. Vårt fokus ligger på att hitta just de som vill utvecklas och se möjligheter.

Sökord: däck , fordon , bilar , mekaniker

Det är en jobbannons. Du är en expert på att analysera jobbannonser och att skriva texter begripligt på lättläst svenska.
Identifiera meningar som är svåra att förstå och skriv om de meningar som inte är skrivna på lättläst svenska
så att de blir enklare att förstå.
Ett värde sätts i resultatet: lattlast_mening: en array som innehåller den ursprungliga meningen
och den mening som är omskriven på lättläst svenska.
Ursprungliga meningar och omskrivna meningar ska vara avskilda med =>

Så här ser resultatet ut av att bearbeta annonsen i enlighet med detta:
