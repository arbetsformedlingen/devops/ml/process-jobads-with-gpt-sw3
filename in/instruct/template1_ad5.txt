<|endoftext|><s>
User:
Sjuksköterskor till kardiologisk vårdavdelning Om oss
Bemanning med omsorg.

Ofelia vård månar om den svenska sjukvården, våra medborgare, om våra sjuksköterskor och övrig vårdpersonal. Vi är Sveriges tryggaste bemanningsföretag för dig som är sjuksköterska. Vi har uppdragen som du söker och villkoren som du förtjänar.
Ofelia vård är ett auktoriserat bemanningsföretag med medlemskap i Kompetensföretagen. Detta är en garanti att vi följer arbetsmarknadens lagar och regler. Vi har kollektivavtal med Vårdförbundet, Läkarförbundet och Unionen. Detta är en självklarhet för oss!
Med en VD och ägare som har lång erfarenhet som anestesisjuksköterska och erfarna konsultchefer med vårderfarenhet så vet vi vad som är viktigt för dig som konsult ute på uppdrag. Vi är upphandlade i hela Sverige och finns tillgängliga för dig dygnet runt.
Ofelia vård finns här för dig som vill arbeta för de villkor som du förtjänar, den arbetsmiljö som du vill ha och den flexibilitet som livet ibland kräver.



Dina arbetsuppgifter
Vi söker nu flera sjuksköterskor till en kardiologisk vårdavdelning. På avdelningen vårdas främst patienter med hjärtinfarkt, rytmrubbningar och hjärtsvikt. Hjärtövervakningsmöjlighet, telemetri finns till alla 15 vårdplatser. Patienterna vårdas i enkelrum. Avdelningen är uppdelad i tre vårdlag och i samband med Covid-19 startades en lätt HIA-avdelning där två patienter kan vårdas.
Sedvanliga arbetsuppgifter.

Passa på att njuta av närheten till havet och det stora utbudet av konst Gotland har att erbjuda.

Kontakta oss gärna på info@ofelia.se/010-178 36 20 för mer information.

Vi hjälper självfallet till med resor och boende vid behov.

Din profil
Vi söker dig som är legitimerad sjuksköterska med relevant erfarenhet.

Varmt välkommen med dina önskemål och din ansökan redan idag!

Det är en jobbannons. Sammanfatta det viktigaste i den.
<s>
Bot:
