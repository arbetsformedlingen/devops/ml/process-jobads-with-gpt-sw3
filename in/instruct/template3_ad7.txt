<|endoftext|><s>
User:
Sjuksköterskor till allmänpediatrisk vårdavdelning Om oss
Bemanning med omsorg.

Ofelia vård månar om den svenska sjukvården, våra medborgare, om våra sjuksköterskor och övrig vårdpersonal. Vi är Sveriges tryggaste bemanningsföretag för dig som är sjuksköterska. Vi har uppdragen som du söker och villkoren som du förtjänar.
Ofelia vård är ett auktoriserat bemanningsföretag med medlemskap i Kompetensföretagen. Detta är en garanti att vi följer arbetsmarknadens lagar och regler. Vi har kollektivavtal med Vårdförbundet, Läkarförbundet och Unionen. Detta är en självklarhet för oss!
Med en VDoch ägaresom har lång erfarenhet som anestesisjuksköterska och erfarna konsultchefer med vårderfarenhet så vet vi vad som är viktigt för dig som konsult ute på uppdrag. Vi är upphandlade i hela Sverige och finns tillgängliga för dig dygnet runt.
Ofelia vård finns här för dig som vill arbeta för de villkor som du förtjänar, den arbetsmiljö som du vill ha och den flexibilitet som livet ibland kräver.



Dina arbetsuppgifter
Sedvanliga arbetsuppgifter för sjuksköterska på pediatrisk vårdavdelning.

Kontakta oss gärna för mer på information på info@ofelia.se/010-178 36 20

Vi hjälper självfallet till med resa och boende vid behov.

Din profil
Vi söker dig som är legitimerad sjuksköterska med relevant erfarenhet.

Varmt välkommen med dina önskemål och din ansökan redan idag!

Det är en jobbannons. Du är en expert på att analysera jobbannonser och att skriva texter begripligt på lättläst svenska.
Identifiera meningar som är svåra att förstå och skriv om de meningar som inte är skrivna på lättläst svenska
så att de blir enklare att förstå.
Ett värde sätts i resultatet: lattlast_mening: en array som innehåller den ursprungliga meningen
och den mening som är omskriven på lättläst svenska.
Ursprungliga meningar och omskrivna meningar ska vara avskilda med =>
<s>
Bot:
