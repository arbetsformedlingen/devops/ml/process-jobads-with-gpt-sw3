<|endoftext|><s>
User:
Rekondare sökes till Riddermark Bil! Om Oss:

Aura Personal söker nu passionerade och engagerade individer för att bli en del av Riddermark Bil, en ledande aktör inom begagnade bilar. Riddermark Bil är känt för sin höga standard och strävan efter att överträffa förväntningarna. Med över 350 medarbetare, 14 anläggningar och mer än 3000 bilar i lager runt om i landet har du möjligheten att vara en del av Sveriges största bolag för begagnade bilar och vara med på deras framgångsresa.

Tjänst:

Bilrekondare

Kvalifikationer:

Vi söker nu en självgående och motiverad Bilrekondare som trivs i en dynamisk miljö. Om du är noggrann, kommunikativ, strukturerad och har ett öga för detaljer, kan du vara den person vi letar efter. Följande kvalifikationer är viktiga för att lyckas i rollen:

- Ansvarsfull, engagerad och drivmotiverad.
- Stresstålig och kapabel att arbeta effektivt under tidspress.
- Goda kommunikationsfärdigheter i svenska/engelska.
- B-Körkort är meriterande.

Arbetsbeskrivning:

I rollen som Bilrekondare kommer du att spela en central roll i att förbereda fordonen för försäljning genom att utföra rengöring och förbättringar. Ditt arbete kommer bidra till att höja kvaliteten och synligheten av Riddermark Bils fordon på marknaden.

Placering:

Du kommer initialt vara baserad i Kungsängen fram till kvartal 1 2024. Därefter kommer verksamheten flytta till nya toppmoderna lokaler i Strängnäs.

Ansökan:

Om du är redo för en spännande utmaning och tror att du har de kvalifikationer vi söker, skicka in din ansökan snarast. Detta är en tills vidare heltidstjänst med omgående anställning.

Vid frågor om tjänsten, kontakta konsultchef Waltteri Tukonen på 070 597 45 47.

Vi ser fram emot att välkomna en engagerad och kompetent Bilrekondare till Riddermark Bil!

Det är en jobbannons. Du är en expert på att analysera jobbannonser och ge förslag på hur en annons kan skrivas bättre,
för att attrahera kvinnliga sökande. En annons anses ha hög kvalitet om den attraherar kvinnliga sökande.
Detta innebär att du kan ge förslag på hur en text som är skriven med en alltför manlig text kan skrivas på ett mer kvinnligt sätt.
Du processar annonsen och ger tillbaka två värden: annons_ok: true|false (indikerar om redan har tillräckligt hög kvalitet eller inte),
och forbattringsforslag: en array som är tom om annonsen har tillräckligt hög kvalitet,
och annars innehåller de delsträngar från annonsen som borde skrivas om, samt vad de borde skrivas om till för att få hög kvalitet.
Delsträngarna ska vara avskilda med =>
<s>
Bot:
