<|endoftext|><s>
User:
Senior Java Systemutvecklare – Oslo heltid Modernera är ett modernt rekryterings- och bemanningsföretag. www.modernera.se
För en kunds räkning söker vi nu en senior Java systemutvecklare. Vår kund levererar lösningar som gör att deras kunder kan öka förändringstakt och kvalitet i sina digitala tjänster. Företagets kunder finns främst bland teleoperatörer, offentlig sektor, bank och försäkring i Sverige och Norge. Företaget har funnits sedan 1999 och består idag av ett 50-tal medarbetare.
Du kommer att jobba i trevliga lokaler i centrala Oslo. Det finns möjlighet att arbeta hemifrån.
Du kommer att arbeta i varierande projekt och stå inför intressanta utmaningar för att effektivisera företagets kunders tekniska vardag.
Du har:
Utbildning på kandidatnivå
Minst 3 års erfarenhet inom utveckling
Backend programmering (Java)
CI/CD pipeline (som Jenkins, Bamboo, Tekton)
Versionshantering (som Git, SVN, BitBucket)
Meriterande:
Frontend programering (som TypeScript, react.js, Node.js, angular.js)
Arkitektur baserad på mikrotjänster
Docker
Kubernetes
Terraform
OpenShift Cloud (som AWS, Azure)
Python
Shell
C++
Configuration as code
Infrastructure as code
Du får:
Arbeta i utmanande projekt med teknik i framkant
Kontinuerlig kompetensutveckling där certifieringar är en självklarhet
Vara en del av en stimulerande arbetsmiljö med högt i tak
Åka på spännande konferensresor med roliga aktiviteter
Privat sjukvårdsförsäkring, pensionsersättning, ett ordentligt friskvårdsbidrag och andra generösa förmåner
Du kommer att bli anställd hos vår kund. Heltid. Provanställning som övergår i en tillsvidareanställning efter sex månader. Tillträde: omgående.
Ansök genom att skicka ditt CV och personligt brev till info@modernera.se. Skriv ”Senior Java Systemutvecklare – Oslo heltid” i ärenderaden.
Vi söker en ninja som vill jobba hos oss.
Vi vill helst att du inte har fyllt 50 år. Du får heller inte heta Test Testsson.

Det är en jobbannons. Du är en expert på att analysera jobbannonser och ge förslag på hur en annons kan skrivas bättre,
för att attrahera kvinnliga sökande. En annons anses ha hög kvalitet om den attraherar kvinnliga sökande.
Detta innebär att du kan ge förslag på hur en text som är skriven med en alltför manlig text kan skrivas på ett mer kvinnligt sätt.
Två värden sätts i resultatet: annons_ok: true|false (indikerar om redan har tillräckligt hög kvalitet eller inte),
och forbattringsforslag: en array som är tom om annonsen har tillräckligt hög kvalitet,
och annars innehåller de delsträngar från annonsen som borde skrivas om, samt vad de borde skrivas om till för att få hög kvalitet.
Delsträngarna ska vara avskilda med =>
<s>
Bot:
