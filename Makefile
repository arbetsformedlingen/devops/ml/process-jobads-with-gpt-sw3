result-dir    = /var/tmp/results-process-jobads-with-gpt-sw3
disk-size     = 350
region        = us-west-2

# x86_64
ami           = ami-0c29f8386758cadf1
instance-type = g4dn.2xlarge

# arm
#ami           = ami-096319086cc3d5f23
#instance-type = inf2.xlarge


.PHONY: launch harvest mk-ads mk-input

launch:
	launchotron --result /home/ubuntu/process-jobads-with-gpt-sw3/out --upload start_aws.sh --region-name $(region) --disk-size $(disk-size) --instance-type $(instance-type) --image $(ami) ./start_aws.sh

harvest: $(result-dir)
	next_number=$$(find "$(result-dir)" -maxdepth 1 -type d -name '[0-9]*' | grep -o '[0-9]*' | sort -n | tail -n 1) \
	&& next_number=$$((next_number + 1)) \
	&& mkdir -p "$(result-dir)"/$$next_number \
	&& time launchotron --outputdir "$(result-dir)"/$$next_number --harvest

$(result-dir):
	mkdir -p "$(result-dir)"

mk-input:
	. venv/bin/activate && python3 src/gen_prompts.py --output-dir in --ad-dir data/ads --template-dir data/prompt_templates

mk-ads: data/ads
	. venv/bin/activate && python3 src/gen_ad_files.py --output-dir data/ads --num-ads 10

data/ads:
	mkdir -p data/ads