import os
from pathlib import Path
import requests
import random
import argparse
from datetime import datetime, timedelta
from langdetect import detect


parser = argparse.ArgumentParser(description='gen_ad_files')
parser.add_argument('--output-dir', default=".", help='Output directory')
parser.add_argument('--num-ads', default=2, help='Number of ads')
args = parser.parse_args()


def get_ads(nr: int):
    before = datetime.today() - timedelta(hours=24, minutes=0)
    some_hours_ago = before.strftime('%Y-%m-%dT%H:%M:%S')

    # Plocka en slumpmässigt vald annons från brandslangen
    url = 'https://jobstream.api.jobtechdev.se/stream'
    params = {'date': some_hours_ago}
    headers = {'accept': 'application/json'}

    response = requests.get(url, params=params, headers=headers)
    data = response.json()
    filtered_data = [item for item in data if "description" in item and "text" in item["description"] and "headline" in item]
    jobad_texts = [jobad['headline'] + ' ' + jobad['description']['text'] for jobad in filtered_data]
    long_jobad_texts = [jobad for jobad in jobad_texts if len(jobad.split()) > 200 and len(jobad.split()) < 300]
    swedish_jobad_texts = [jobad for jobad in long_jobad_texts if detect(jobad) == "sv"]
    jobads = random.sample(swedish_jobad_texts, nr)
    return jobads

ads = get_ads(int(args.num_ads))

# die unless we got the expected number of ads
assert len(ads) == int(args.num_ads)

try:
    file_list = list(Path(args.output_dir).glob("*.txt"))
    file_list.sort(key=lambda x: int(os.path.splitext(x.name)[0][2:]))
    last_file = file_list[-1].name

    # determine the numeric filename of the next ad file
    next_file = int(last_file[2:-4]) + 1
except IndexError:
    next_file = 0


# write the ads to files
for i, ad in enumerate(ads):
    with open(f"{args.output_dir}/ad{next_file + i}.txt", "w", encoding="utf-8") as f:
        f.write(ad)
