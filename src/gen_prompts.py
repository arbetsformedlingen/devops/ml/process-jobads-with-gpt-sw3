import os
import argparse


parser = argparse.ArgumentParser(description='gen_prompts')
parser.add_argument('--output-dir', default=".", help='Output directory')
parser.add_argument('--template-dir', default=2, help='Template directory')
parser.add_argument('--ad-dir', default=2, help='Ad directory')
args = parser.parse_args()


def get_ads(ad_dir: str):
    # read the ads from files in the ad directory
    ads = []
    for file in os.listdir(ad_dir):
        with open(f"{ad_dir}/{file}", "r", encoding="utf-8") as f:
            ads.append(f.read())
    return ads

def get_templates(template_dir: str):
    # read the templates from files in the template directory
    templates = []
    for file in os.listdir(template_dir):
        with open(f"{template_dir}/{file}", "r", encoding="utf-8") as f:
            templates.append(f.read())
    return templates


ads = get_ads(args.ad_dir)
completion_templates = get_templates(f"{args.template_dir}/completion")
instruct_templates = get_templates(f"{args.template_dir}/instruct")

for i, template in enumerate(completion_templates):
    for j, ad in enumerate(ads):
        with open(f"{args.output_dir}/completion/template{i}_ad{j}.txt", "w", encoding="utf-8") as f:
            f.write(template.replace("{{{JOB_AD_TEXT}}}", ad))

for i, template in enumerate(instruct_templates):
    for j, ad in enumerate(ads):
        with open(f"{args.output_dir}/instruct/template{i}_ad{j}.txt", "w", encoding="utf-8") as f:
            f.write(template.replace("{{{JOB_AD_TEXT}}}", ad))
