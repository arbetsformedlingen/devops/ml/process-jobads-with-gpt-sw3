import sys
import torch
import argparse
from pathlib import Path
from transformers import StoppingCriteriaList, StoppingCriteria
from transformers import pipeline, AutoTokenizer, AutoModelForCausalLM


# Initialize Variables
default_model_name = "AI-Sweden-Models/gpt-sw3-6.7b-v2-instruct-4bit-gptq"
default_out_dir = "out"
default_in_dir = "in"
device = "cuda:0" if torch.cuda.is_available() else "cpu"


# Create a parser
parser = argparse.ArgumentParser(description='gptscript')
parser.add_argument('--model', default=default_model_name, help='Model name to use')
parser.add_argument('--out_dir', default=default_out_dir, help='Output directory')
parser.add_argument('--in_dir', default=default_in_dir, help='Input prompt file directory')
args = parser.parse_args()

model_name = args.model
out_dir = args.out_dir
in_dir = args.in_dir


# Initialize Tokenizer & Model
tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForCausalLM.from_pretrained(model_name)
model.eval()
model.to(device)


# (Optional) - define a stopping criteria
# We ideally want the model to stop generate once the response from the Bot is generated
class StopOnTokenCriteria(StoppingCriteria):
    def __init__(self, stop_token_id):
        self.stop_token_id = stop_token_id

    def __call__(self, input_ids, scores, **kwargs):
        return input_ids[0, -1] == self.stop_token_id

stop_on_token_criteria = StopOnTokenCriteria(stop_token_id=tokenizer.bos_token_id)


# create a function which accepts a filename as argument
def chat(file_name):
    # get name of a file matching name *.txt in subdirectory "in" relative to the current file
    with open(file_name, "r", encoding="utf-8") as f:
        prompt = f.read()
    prompt = prompt.strip()

    input_ids = tokenizer(prompt, return_tensors="pt")["input_ids"].to(device)

    generated_token_ids = model.generate(
        inputs=input_ids,
        max_new_tokens=256,
        do_sample=True,
        temperature=0.85,
        top_p=1,
        stopping_criteria=StoppingCriteriaList([stop_on_token_criteria])
    )[0]

    generated_text = tokenizer.decode(generated_token_ids[len(input_ids[0]):-1])
    return generated_text

# foreach file matching name *.txt in subdirectory "in" relative to the current directory, call the function
# and write the result to a file with the same name in out_dir subdirectory relative to the current directory
for file_name in Path(in_dir).glob("*.txt"):
    with open(f"{out_dir}/{file_name.name}", "w", encoding="utf-8") as f:
        f.write(chat(file_name))
