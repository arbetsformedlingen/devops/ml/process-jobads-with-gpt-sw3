Psykolog online - heltid remote Är du en legitimerad psykolog med steg 1 eller 2 i Kognitiv Beteendeterapi (KBT) med minst ett års erfarenhet av behandling?
Letar du efter en flexibel heltidsanställning som ger dig en balans mellan arbetsliv och fritid?
Trivs du med att arbeta med behandling och fokusera på terapeutisk allians?
Om svaret är ja på ovanstående frågor, fortsätt läsa för att se om rollen som onlinepsykolog hos oss på Din Psykolog skulle kunna passa dig.
Om oss
Din Psykolog är en digital psykologmottagning som erbjuder effektiv och högkvalitativ psykologisk hjälp. Våra psykologer utför terapi via videosamtal och behandlar klienter med mild till måttlig psykisk ohälsa. Vi lägger stor vikt vid att våra klienter känner sig trygga, hörda och sedda i kontakten med våra psykologer.
Hos Din Psykolog får du arbeta utefter ett schema som passar dig. Du kan arbeta var du vill. Arbetet utförs på distans och vi erbjuder intern och extern handledning.
Arbetsuppgifter
Genom videosamtal och med stöd av IKBT kommer du att genomföra bedömningar och ge psykologisk behandling till klienter med mild till måttlig psykisk ohälsa.
Din profil
Legitimerad psykolog med steg 1 eller 2 i KBT
Minst ett års erfarenhet av behandling på heltid
Goda datorkunskaper
Folkbokförd i Sverige
Fördelar med att arbeta på Din Psykolog
Fokus på behandling
Distansarbete
6h arbetsdag+flexibelt schema
Digital fika med kollegorna
Extern+intern handledning
Workshops
Vi ser fram emot din ansökan!
Öppen för alla
Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.