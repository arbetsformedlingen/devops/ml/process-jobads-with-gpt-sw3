Kundmottagare till Göteborg Vi söker kundmottagare till vår kund i Göteborg med start omgående!

Du kommer vara ansiktet utåt på vår kunds däckverkstad i Göteborg. Din primära uppgift kommer vara att säkerställa god kundnöjdhet och service. Tjänsten omfattar varierande arbetsuppgifter men kundkontakt kommer vara återkommande i arbetet.

I arbetet ingår även uppgifter som att

- Sälja däck
- Hantera fakturor
- Administration
- Kundrelationer
- Rekommendationer av däck

Som person behöver du kunskap om service och kundrelationer, vi ser gärna att du tidigare har erfarenhet som kundmottagare eller likande roll. Tjänsten kräver stort ansvar eftersom du kommer jobba med rådgivning och försäljning. Krav för tjänsten är b – körkort, servicemindeed, arbetsvillig och lösningsorienterad. Personlighet värderas högt och tillsättning sker löpande, så skicka in din ansökan direkt!

Vi erbjuder spännande arbetsuppgifter hos ett väletablerat företag med stora utvecklingsmöjligheter. Tjänsten är på heltid under höstsäsong där du kommer arbeta måndag – fredag under kontorstider.       

Vill du veta mer om företaget och tjänsten, skicka in din ansökan nu så berättar vi gärna mer.

Om oss:
Aura personal tror på människor och att alla som vill kan. Våra år i branschen har gjort oss övertygade om att många fler kan skapa sin egen framgång bara någon ser dem och ger dem chansen. Vårt fokus ligger på att hitta just de som vill utvecklas och se möjligheter.