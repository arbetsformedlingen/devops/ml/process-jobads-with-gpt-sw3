#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

WORKSPACE=/workspace

trap "touch /tmp/.finished" EXIT

mkdir -p "$WORKSPACE"/out/instruct

cd "$WORKSPACE"

curl https://gitlab.com/arbetsformedlingen/devops/ml/process-jobads-with-gpt-sw3/-/archive/main/process-jobads-with-gpt-sw3-main.tar.gz |\
    tar --strip-components=1 -zxf -

python3 -m venv venv
source venv/bin/activate

pip install -r requirements.txt

# for saving vast ai envvars from the init shell
env | grep _ >> /etc/environment

time python chat.py --in_dir "$WORKSPACE"/in/instruct --out_dir "$WORKSPACE"/out/instruct --model "AI-Sweden-Models/gpt-sw3-126m-instruct" > "$WORKSPACE"/out/output.txt 2>"$WORKSPACE"/out/error.txt

##  When everythings fine - use this to shutdown automatically
# pip install vastai
# vastai destroy instance $CONTAINER_ID
