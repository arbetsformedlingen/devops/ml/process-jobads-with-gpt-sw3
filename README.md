## gen_ad_files.py

This Python script is used to generate advertisement files. It fetches job advertisements from an API, filters them based on certain criteria, and writes them to text files.

### Installation
```
pip3 install -r requirements-dev.in
```


### Usage

The script accepts two command-line arguments:

- `--output-dir`: The directory where the generated advertisement files will be saved. The default value is the current directory.
- `--num-ads`: The number of advertisements to fetch and save. The default value is 2.

### Functionality

The script works as follows:

1. It fetches job advertisements from the API at 'https://jobstream.api.jobtechdev.se/stream'. The API is queried with a date parameter set to two hours before the current time.

2. The fetched data is filtered to include only those items that have a "description" field containing "text" and a "headline" field.

3. The script concatenates the headline and the text of the description for each item and filters out those that have less than 100 characters.

4. The script then filters out those advertisements that are not in Swedish.

5. The script randomly selects a number of advertisements equal to the `num-ads` argument.

6. The script then checks the output directory for existing files and determines the numeric filename of the next advertisement file.

7. Finally, the script writes the selected advertisements to text files in the output directory. Each advertisement is written to a separate file.

### Example

To generate 5 advertisement files in the directory `./ads`, you would run the script as follows:

```bash
python3 gen_ad_files.py --output-dir ./ads --num-ads 5
```

This will create 5 text files in the `./ads` directory, each containing a single job advertisement.
