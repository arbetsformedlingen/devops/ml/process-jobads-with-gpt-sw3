#!/bin/bash

git clone https://gitlab.com/arbetsformedlingen/devops/ml/process-jobads-with-gpt-sw3.git
cd process-jobads-with-gpt-sw3
mkdir out

. /opt/conda/etc/profile.d/conda.sh
conda activate /opt/conda/envs/pytorch

# quantized model
pip install transformers sentencepiece optimum auto-gptq
pip install gpustat

#sudo apt install nvtop
#sudo nvtop


models="\
AI-Sweden-Models/gpt-sw3-126m \
AI-Sweden-Models/gpt-sw3-356m \
AI-Sweden-Models/gpt-sw3-1.3b \
AI-Sweden-Models/gpt-sw3-6.7b \
AI-Sweden-Models/gpt-sw3-6.7b-v2 \
AI-Sweden-Models/gpt-sw3-20b \
AI-Sweden-Models/gpt-sw3-40b"
models_instruct="\
AI-Sweden-Models/gpt-sw3-126m-instruct \
AI-Sweden-Models/gpt-sw3-356m-instruct \
AI-Sweden-Models/gpt-sw3-1.3b-instruct \
AI-Sweden-Models/gpt-sw3-6.7b-v2-instruct \
AI-Sweden-Models/gpt-sw3-20b-instruct"
models_instruct_quantized="\
AI-Sweden-Models/gpt-sw3-6.7b-v2-instruct-4bit-gptq \
AI-Sweden-Models/gpt-sw3-20b-instruct-4bit-gptq"

cat << "EOF" >/tmp/stat.sh
while true; do
	gpustat --no-color --json | jq -rc '.gpus[] | [."memory.used", ."memory.total"] | join(",")'
	sleep 1
done
EOF
chmod a+rx /tmp/stat.sh

df -h | head -n 1 >out/df.log

function run_tests() {
    local in_dir="$1"; shift
    local base_out_dir="$1"; shift
    local mdls="$@"

    for m in $mdls; do
        out_dir="$base_out_dir/$m"
        mkdir -p "$out_dir"
        bash /tmp/stat.sh > "$out_dir"/stats.log &
        child_pid=$$
        /usr/bin/time -p -o "$out_dir"/time.log /opt/conda/envs/pytorch/bin/python chat.py --in_dir "$in_dir" --out_dir "$out_dir" --model "$m" 2>"$out_dir"/err.log 2>"$out_dir"/out.log
        echo $? > "$out_dir"/exitstatus.log
        pkill -P "$child_pid"
        { echo -ne "$m]\t"; df -h | grep root; } >> out/df.log
        find  ~/.cache/huggingface/hub/ -maxdepth 1 -mindepth 1 -exec rm -rf {} \;
    done
}

run_tests in/completion out/completion $models
run_tests in/instruct out/instruct $models_instruct $models_instruct_quantized

cp ~/out.log out/job.log
